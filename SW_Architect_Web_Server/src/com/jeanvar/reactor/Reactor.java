/**
 * @namespace		com.jeanvar.reactor
 * @brief			Reactor 패턴의 core부분
 * @details
 * 	Reactor 방식의 웹서버에서 요청을 처리하는 Reactor
 * 	헤더에 있는 핸들을 통해 알맞은 핸들러를 반환하는 Demultiplexer
 * 	Reactor 구동 시 필요한 클래스 로딩 등의 설정을 담당하는 InitConfiguration
 */
package com.jeanvar.reactor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;
import com.jeanvar.initiator.WebServer;

/**
 * @brief		Client로부터 요청을 받아서 이를 수행할 event handler를 찾고 event 처리를 위임하는 Reactor 클래스
 * @details
 * 	Reactor 클래스는 Client로부터 요청을 받아서 Demultiplexer를 통해 이를 처리할 수 있는
 * 	Event Handler를 반환받는다. 그리고 스레드 풀에서 사용 가능한 스레드를 통해 이벤트의 처리를 위임한다.
 * @author 		TaekSoon
 * @date		2014-05-12
 * @version	0.1
 *
 */
public class Reactor implements WebServer {	
	public static Logger logger = Logger.getLogger(Reactor.class.getName());
	
	private final int NUM_OF_THREADS = 10;
	
	private int port;
	private Demultiplexer demux;
	
	public Reactor(int port) {
		this.port = port;
		this.demux = new Demultiplexer();
	}
	
	/**
	 * @brief			클라이언트로부터의 요청을 받아 demultiplexer를 통해 이벤트 핸들러를 찾아  처리를 위임.
	 * @details
	 *  요청을 처리하기위한 스레드 풀 생성. (ExecutorService)
	 * 	클라이언트로부터의 요청을 받기 위해 대기. (accept())
	 *  요청이 들어오면 요청이 들어온 서버 소켓의 참조를 Demultiplexer에게 넘김.
	 *  Demuliplexer는 EventHandler를 반환.
	 *  스레드풀에서 대기 상태인 스레드를 골라 EventHandler를 위임하여 Event 처리.
	 *  @exception		IOException(ServerSocket 생성)
	 *  
	 */
	public void handleEvents() {
		ExecutorService es = Executors.newFixedThreadPool(NUM_OF_THREADS);
		logger.info(NUM_OF_THREADS + " threads has been made in the thread pool.");
		try {
			ServerSocket ss = new ServerSocket(port);
			while (true) {
				logger.info("Ready to Listen...");
				final Socket cs = ss.accept();
				
				logger.info("Client[Remote : " + cs.getRemoteSocketAddress() + " ] connected.");
				final EventHandler handler = demux.select(cs);
				es.execute(new Runnable() {
					@Override
					public void run() {
						handler.handleEvent(cs);
					}
				});
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	/**
	 * @brief			event를 처리할 event handler 등록
	 * @param 			handle
	 * @param 			handler : event handler
	 */
	public void registerHandler(String handle, EventHandler handler) {
		demux.setHandler(handle, handler);
	}
	
	/**
	 * @brief			handle에 등록된 event handler를 제거.
	 * @param 			handle
	 */
	public void removeHandler(String handle) {
		demux.removeHandler(handle);
	}

	@Override
	public void start() {
		handleEvents();
	}
}
