/**
 * @namespace		com.jeanvar.reactor
 * @brief			Reactor 패턴의 core부분
 * @details
 * 	Reactor 방식의 웹서버에서 요청을 처리하는 Reactor
 * 	헤더에 있는 핸들을 통해 알맞은 핸들러를 반환하는 Demultiplexer
 * 	Reactor 구동 시 필요한 클래스 로딩 등의 설정을 담당하는 InitConfiguration
 */
package com.jeanvar.reactor;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

/**
 * @brief		Handle에 맞는 EventHandler를 반환하는 클래스
 * @details
 * 	 Socket의 Stream에서 헤더 부분만 파싱해서 가지고 있던 handletable에서
 *   알맞은 handler를 찾아 반환한다.
 * @author 		TaekSoon
 * @date		2014-05-12
 * @version	0.1
 *
 */
public class Demultiplexer {
	
	public Logger logger = Logger.getLogger(Demultiplexer.class.getName());
	
	private final int HEADER_SIZE = 6;
	private Map<String, EventHandler> handleTable;
	
	public Demultiplexer() {
		handleTable = new HashMap<String, EventHandler>();
	}
	
	/**
	 * @brief		Socket의 Stream에서 핸들을 파싱해서 알맞은 핸들러를 반환하는 메서드
	 * @details
	 * 	인자로 받은 Socket의 Stream을 통해 Header size만큼의 문자열을 파싱
	 *  해당 문자열이 Handle에 해당되고 이를 이용해서 Handle Table에서 적절한 Handler를 반환함.
	 * @param 		clientSocket : 클라이언트로부터의 요청을 받은 소켓
	 * @return		handler : handle에 맞는 Eventhandler 반환. (없으면 null)
	 * @exception	IOException (Socket으로부터 inputstream을 얻어옴)
	 */
	public EventHandler select(Socket clientSocket) {
		EventHandler handler = null;
		try {
			InputStream is = clientSocket.getInputStream();
			byte[] buf = new byte[HEADER_SIZE];
			is.read(buf);
			String handle = new String(buf);
			logger.info("Received Header : " + handle);
			handler = getEventHandler(handle);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			return handler;
		}
	}
	
	/**
	 * @brief		handle table에서 handle을 이용해 알맞은 handler를 반환.
	 * @param 		handle
	 * @return		eventHandler
	 */
	private EventHandler getEventHandler(String handle) {
		return handleTable.get(handle);
	}
	
	/**
	 * @brief		handle에 맞는 event handler를 handle table에 등록함.
	 * @param 		handle
	 * @param 		handler : event handler
	 */
	public void setHandler(String handle, EventHandler handler) {
		handleTable.put(handle, handler);
	}
	
	/**
	 * @brief		handle에 등록된 event handler를 제거함.
	 * @param 		handle
	 */
	public void removeHandler(String handle) {
		handleTable.remove(handle);
	}
}
