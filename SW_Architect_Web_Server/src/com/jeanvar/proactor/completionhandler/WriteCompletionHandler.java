/**
 * @namespace			com.jeanvar.proactor.completionhandler
 * @brief				Async I/O작업이 끝난 뒤 실행될 CompletionHandler의 패키지.
 * @details
 * 	 Accept, Read, Write의 비동기 I/O 작업이 끝난 뒤 Completion Queue에 저장되면
 *   이 패키지 안의 각각의 CompletionHandler 클래스가 호출되어 이벤트를 처리한다.
 */
package com.jeanvar.proactor.completionhandler;

import java.io.IOException;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import org.apache.log4j.Logger;

/**
 * @brief			Write I/O가 끝났을 때의 콜백 메서드를 정의한 클래스.
 * @details
 *    비동기 Write I/O가 성공하면 성공 로그를 남기고 클라이언트 소켓을 닫는다.
 *    실패할 경우 에러 로그를 남기고 클라이언트 소켓을 닫는다.
 * @author 			TaekSoon
 * @date			2015-05-19
 * @version		0.1
 */
public class WriteCompletionHandler implements CompletionHandler {
	
	public Logger logger = Logger.getLogger(WriteCompletionHandler.class.getName());
	
	private AsynchronousSocketChannel socketChannel;
	
	public WriteCompletionHandler(AsynchronousSocketChannel socketChannel) {
		this.socketChannel = socketChannel;
	}
	
	/**
	 * @brief			Async Write I/O가 성공하면 실행되는 콜백 메서드.
	 * @details
	 *    Write I/O가 성공했음을 로그로 남기고 클라이언트 소켓을 닫는다.
	 *    클라이언트 소켓 닫기에 실패하면 로그를 남긴다.
	 * @param			특별히 전달받을 파라미터가 없음.
	 * 
	 */
	@Override
	public void completed(Object result, Object attachment) {
		try {
			logger.info("Write I/O to client Finished.");
			logger.info("Closing client socket....");
			socketChannel.close();
			logger.info("Client socket closed.");
		} catch (IOException e) {
			logger.error("Client socket cannot be closed.");
			e.printStackTrace();
		}
	}
	
	/**
	 * @brief			Async Write I/O 실패시 실행되는 콜백 메서드.
	 * @details
	 *    클라이언트 소켓을 닫고 에러 로그를 출력한다.
	 *    클라이언트 소켓 닫기에 실패했을 경우 해당 에러 로그를 출력한다.
	 * @param			특별히 전달받을 파라미터가 없음.
	 * 
	 */
	@Override
	public void failed(Throwable exc, Object attachment) {
		try {
			logger.error("Write to client failed.");
			logger.info("Closing client socket....");
			socketChannel.close();
			logger.info("Client socket closed.");
		} catch (IOException e) {
			logger.error("Client socket cannot be closed.");
			e.printStackTrace();
		}
	}

}
