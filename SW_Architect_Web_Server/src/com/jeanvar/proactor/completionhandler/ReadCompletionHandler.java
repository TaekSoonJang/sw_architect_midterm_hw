/**
 * @namespace			com.jeanvar.proactor.completionhandler
 * @brief				Async I/O작업이 끝난 뒤 실행될 CompletionHandler의 패키지.
 * @details
 * 	 Accept, Read, Write의 비동기 I/O 작업이 끝난 뒤 Completion Queue에 저장되면
 *   이 패키지 안의 각각의 CompletionHandler 클래스가 호출되어 이벤트를 처리한다.
 */
package com.jeanvar.proactor.completionhandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;
import com.jeanvar.proactor.Demultiplexer;

/**
 * @brief			Read I/O가 끝났을 때의 콜백 메서드를 정의한 클래스.
 * @details
 *    비동기 Read I/O가 성공하면 헤더와 본문을 나눠서 읽고, 헤더에 맞는 이벤트 핸들러를 설정한다.
 *    실패할 경우 클라이언트 소켓을 닫고 에러 로그를 남긴다.
 * @author 			TaekSoon
 * @date			2015-05-19
 * @version		0.1
 */
public class ReadCompletionHandler implements CompletionHandler<Integer, Demultiplexer> {
	
	public Logger logger = Logger.getLogger(ReadCompletionHandler.class.getName());
	
	private static int HEADER_SIZE = 6;
	
	private AsynchronousSocketChannel socketChannel;
	private ByteBuffer buffer;
	
	public ReadCompletionHandler(AsynchronousSocketChannel socketChannel, ByteBuffer buffer) {
		this.socketChannel = socketChannel;
		this.buffer = buffer;
	}
	
	/**
	 * @brief			Async Read I/O가 성공하면 실행되는 콜백 메서드.
	 * @details
	 *    Read I/O의 결과가 담긴 버퍼를 두번에 나누어 읽어서 Header와 Body를 분리해냄.
	 *    Header를 이용해 핸들 테이블에서 이벤트 핸들러를 전달받고, 여기에 Body를 보내서
	 *    클라이언트에게 반환할 응답 메세지를 만듦.
	 *    마지막으로 응답 버퍼를 만들고 Write Completion Handler에 전달 후 Write I/O 실행.
	 * @param			bytesRead	:	클라이언트로부터 읽은 메시지의 크기
	 * @param			demux		:	헤더 메시지 안에 담긴 핸들과 이에 맞는 핸들러를 가지고 있는 테이블.
	 * 
	 */
	@Override
	public void completed(Integer bytesRead, Demultiplexer demux) {
		logger.info(bytesRead + "bytes read.");
		
		buffer.rewind();
		
		byte[] headerBuf = new byte[HEADER_SIZE];
		byte[] bodyBuf = new byte[bytesRead - HEADER_SIZE];
		
		buffer.get(headerBuf);
		String header = new String(headerBuf);
		logger.info("Header : " + header);
		
		buffer.get(bodyBuf);
		String body = new String(bodyBuf);
		logger.info("Body : " + body);
		
		EventHandler handler = demux.getEventHandler(header);
		String responseMsg = (String)handler.handleEvent(body);
		logger.info("Response Msg[" + responseMsg + "] to be written to client...");
		
		ByteBuffer outBuffer = ByteBuffer.wrap(responseMsg.getBytes());
		
		WriteCompletionHandler writeCompletionHandler = new WriteCompletionHandler(socketChannel);
		socketChannel.write(outBuffer, null, writeCompletionHandler);
		logger.info("Async Write I/O Started....");
	}
	
	/**
	 * @brief			Async Read I/O 실패시 실행되는 콜백 메서드.
	 * @details
	 *    클라이언트 소켓을 닫고 에러 로그를 출력한다.
	 *    클라이언트 소켓 닫기에 실패했을 경우 해당 에러 로그를 출력한다.
	 * @param			exc		: 	예외 발생
	 * @param			demux	:	핸들 테이블	
	 */
	@Override
	public void failed(Throwable exc, Demultiplexer demux) {
		try {
			socketChannel.close();
			logger.error("Read I/O Failed.");
		} catch (IOException e) {
			logger.error("ServerSocket cannot be closed.");
			e.printStackTrace();
		}
	}

}
