/**
 * @namespace			com.jeanvar.proactor.completionhandler
 * @brief				Async I/O작업이 끝난 뒤 실행될 CompletionHandler의 패키지.
 * @details
 * 	 Accept, Read, Write의 비동기 I/O 작업이 끝난 뒤 Completion Queue에 저장되면
 *   이 패키지 안의 각각의 CompletionHandler 클래스가 호출되어 이벤트를 처리한다.
 */
package com.jeanvar.proactor.completionhandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import org.apache.log4j.Logger;

import com.jeanvar.proactor.Demultiplexer;

/**
 * @brief			Accept I/O가 끝났을 때의 콜백 메서드를 정의한 클래스.
 * @details
 *    비동기 Accept I/O가 성공하면 본문을 읽기 위한 작업을 수행하고,
 *    실패할 경우 서버 소켓을 닫고 에러 로그를 남긴다.
 * @author 			TaekSoon
 * @date			2015-05-19
 * @version		0.1
 */
public class AcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, Demultiplexer> {
	
	public Logger logger = Logger.getLogger(AcceptCompletionHandler.class.getName());
	
	private static int MSG_MAX_SIZE = 1024;
	private AsynchronousServerSocketChannel listener;
	
	public AcceptCompletionHandler(AsynchronousServerSocketChannel listener) {
		this.listener = listener;
	}
	
	/**
	 * @brief			Async Accept I/O가 성공하면 실행되는 콜백 메서드.
	 * @details
	 *    본문을 읽기 위한 버퍼를 메세지 최대 크기만큼 생성하고 이를 Read Completion Handler에 전달.
	 *    클라이언트 소켓 채널에서 비동기 Read I/O 실행.
	 * @param			socketChannel	:	연결된 클라이언트의 소켓
	 * @param			demux			:	헤더 메시지 안에 담긴 핸들과 이에 맞는 핸들러를 가지고 있는 테이블.
	 * 
	 */
	@Override
	public void completed(AsynchronousSocketChannel socketChannel, Demultiplexer demux) {
		listener.accept(demux, this);
		try {
			logger.info("Client[Local : " + socketChannel.getLocalAddress() + ", Remote : " + socketChannel.getRemoteAddress() + "] connected.");
		} catch (IOException e) {
			logger.error("Cannot read client address.");
			e.printStackTrace();
		}
		
		ByteBuffer inputBuffer = ByteBuffer.allocate(MSG_MAX_SIZE);
		ReadCompletionHandler readCompletionHandler = new ReadCompletionHandler(socketChannel, inputBuffer);
		socketChannel.read(inputBuffer, demux, readCompletionHandler);
		logger.info("Async Read I/O Started....");
	}
	
	/**
	 * @brief			Async Accept I/O 실패시 실행되는 콜백 메서드.
	 * @details
	 *    서버 소켓을 닫고 에러 로그를 출력한다.
	 *    서버 소켓 닫기에 실패했을 경우 해당 에러 로그를 출력한다.
	 * @param			exc		: 	예외 발생
	 * @param			demux	:	핸들 테이블	
	 */
	@Override
	public void failed(Throwable exc, Demultiplexer demux) {
		try {
			listener.close();
			logger.error("Accept I/O Failed.");
		} catch (IOException e) {
			logger.error("ServerSocket cannot be closed.");
			e.printStackTrace();
		}
	}

}
