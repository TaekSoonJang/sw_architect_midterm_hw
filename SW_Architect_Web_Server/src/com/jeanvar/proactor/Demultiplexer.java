/**
 * @namespace			com.jeanvar.proactor
 * @biref				Proactor의 핵심 요소들
 * @details
 *   Proactor 구현에 핵심인 proactor, completion queue, async operation을 담당하는
 *   AsynchronousServerSocketChannel을 구동하는 ProactorInitiator와
 *   이벤트 분배를 담당하는 demultiplexer로 구성.
 */
package com.jeanvar.proactor;

import java.util.HashMap;
import java.util.Map;

import com.jeanvar.eventhandler.EventHandler;


/**
 * @brief			Read 작업이후 본문을 처리할 Event Handler의 분배를 담당하는 클래스
 * @details
 * 	 요청의 헤더 부분에 들어있는 핸들과 해당 요청의 본문을 처리할 Event Handler를 Map으로 관리하는 테이블을 가짐.
 * 	 이 테이블에 핸들과 핸들러를 등록, 삭제하여 관리하고 핸들에 맞는 핸들러를 반환해줌. 
 * @author 			TaekSoon
 * @date			2014-05-19
 * @version		0.1
 *
 */
public class Demultiplexer {
	
	private Map<String, EventHandler> handleTable = new HashMap<String, EventHandler>();
	
	public void registerHandler(String handle, EventHandler handler) {
		handleTable.put(handle, handler);
	}
	
	public void removeHandler(String handle) {
		handleTable.remove(handle);
	}
	
	public EventHandler getEventHandler(String handle) {
		return handleTable.get(handle);
	}

}
