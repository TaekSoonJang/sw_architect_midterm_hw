/**
 * @namespace			com.jeanvar.proactor
 * @biref				Proactor의 핵심 요소들
 * @details
 *   Proactor 구현에 핵심인 proactor, completion queue, async operation을 담당하는
 *   AsynchronousServerSocketChannel을 구동하는 ProactorInitiator와
 *   이벤트 분배를 담당하는 demultiplexer로 구성.
 */
package com.jeanvar.proactor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;
import com.jeanvar.initiator.WebServer;
import com.jeanvar.proactor.completionhandler.AcceptCompletionHandler;

/**
 * @brief			Proactor 구동을 위한 클래스
 * @details
 *   Proactor 패턴에 핵심적인 Async Operation, Completion Event Queue를 포함하는
 *   AsynchronousServerSocketChannel을 생성함.
 *   그리고 Completion 이후 데이터 처리를 위한 Event Handler를 등록함.
 * @author 			TaekSoon
 * @date			2014-05-19
 * @version			0.1
 *
 */
public class Proactor implements WebServer {
	
	public static Logger logger = Logger.getLogger(Proactor.class.getName()); 
	
	private final int port;
	
	private final int poolSize = 90;
	private final int initialSize = 20;
	private final int backlog = 200;
	
	private Demultiplexer demux = new Demultiplexer();
	private AsynchronousChannelGroup group;
	private AsynchronousServerSocketChannel listener;
	private AcceptCompletionHandler acceptCompletionHandler;
	/**
	 * @brief			Proactor 초기화 작업
	 * @details
	 *   AsynchrounousChannelGroup을 싱글 스레드 풀로 생성.
	 *   AsynchronousServerSocketChannel생성.
	 *   Accept 요청에 대한 completion handler에게 서버의 소켓을 넘겨줌.
	 * @param 			port	:	서버가 요청을 듣는 포트.
	 * @throws 			IOException
	 */
	public Proactor(int port) {
		this.port = port;
	}
	
	/**
	 * @brief			Proactor 웹서버를 구동함.
	 * @details
	 *    Async Server Socket Channel이 Accept 요청을 받기 시작함.
	 *    요청이 올 때까지 Async Channel Group은 종료되지않고 대기함.
	 * @throws			InterruptedException	: 	대기중인 스레드가 인터럽트됨.
	 */
	@Override
	public void start() {
		
		ExecutorService executor = Executors.newFixedThreadPool(poolSize);
		
		try {
			group = AsynchronousChannelGroup.withCachedThreadPool(executor, initialSize);
			
			Monitor monitor = new Monitor(executor, 10);
			Thread monitorThread = new Thread(monitor);
			monitorThread.start();
			
			logger.info("Multi thread channel group has been made...");
			listener = AsynchronousServerSocketChannel.open(group).bind(new InetSocketAddress(port));
			acceptCompletionHandler = new AcceptCompletionHandler(listener);
			
			listener.accept(demux, acceptCompletionHandler);
			logger.info("Async Server Listening on port : " + port);
			
			logger.info("Waiting for connection from client....");
			group.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (IOException e1) {
			logger.error("Async Server Socket Channel open failed.");
			e1.printStackTrace();
		} catch (InterruptedException e) {
			logger.info("Waiting thread has been interrupted.");
			e.printStackTrace();
		}
	}

	@Override
	public void registerHandler(String handle, EventHandler handler) {
		demux.registerHandler(handle, handler);
	}
}
