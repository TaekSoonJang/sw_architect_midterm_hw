/**
 * @namespace		com.jeanvar.initiator
 * @brief			웹서버를 초기화해서 구동시키는데 필요한 요소들을 모은 패키지.
 * @details
 * 	서버를 띄우고 해당 서버에 여러번의 요청을 보내는 클라이언트를 생성.
 * 	콘솔에 출력되는 결과를 통해 작동 확인.
 *  동적으로 웹서버에서 필요한 클래스들을 로드하는데 필요한 파서.
 */
package com.jeanvar.initiator;

import java.io.File;

import org.apache.log4j.Logger;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.jeanvar.initiator.parser.WebServerParser;

/**
 * @brief		서버를 초기화하고 구동하는 클래스.
 * @details	
 * 	 서버를 각종 설정으로 초기화하고 구동함.
 * @author 		TaekSoon
 * @date		2014-05-19
 * @version	0.1
 * 
 */
public class ServerInitiator {
	
	public static Logger logger = Logger.getLogger(ServerInitiator.class.getName());
	
	private WebServer webServer;
	private String configurationXmlPath;
	private ClassLoader classLoader = new ClassLoader();
	
	public ServerInitiator(WebServer webServer, String configurationXmlPath) {
		this.webServer = webServer;
		this.configurationXmlPath = configurationXmlPath;
	}
	
	/**
	 * @brief			서버를 설정하는 필요한 메서드를 순차적으로 실행함.
	 * @details
	 * 	  현재는 XML파일을 파싱해서 동적으로 이벤트 핸들러를 등록하는 메서드만 존재함.
	 * 
	 */
	public void configure() {
		classLoader.registerHandlerFromXML(webServer, configurationXmlPath);
	}
	
	/**
	 * @brief			웹서버를 구동시키는 메서드.
	 * 
	 */
	public void start() {
		webServer.start();
	}
	
	public static void main(String[] args) throws Exception {
		
		Serializer serializer = new Persister();
		File sortConfigXML = new File("/Users/TaekSoon/git/sw_architect_midterm_hw/SW_Architect_Web_Server/resources/configuration/WebServerPatternConfiguration.xml");
		
		WebServerParser parser;
		try {
			parser = serializer.read(WebServerParser.class, sortConfigXML);
			ServerInitiator si = new ServerInitiator((WebServer)Class.forName(parser.getWebServerClass()).getDeclaredConstructor(int.class).newInstance(5000),
					  parser.getHandlerConfigurationPath());
			si.configure();
			si.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
