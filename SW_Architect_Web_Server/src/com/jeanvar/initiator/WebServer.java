/**
 * @namespace		com.jeanvar.initiator
 * @brief			웹서버를 초기화해서 구동시키는데 필요한 요소들을 모은 패키지.
 * @details
 * 	서버를 띄우고 해당 서버에 여러번의 요청을 보내는 클라이언트를 생성.
 * 	콘솔에 출력되는 결과를 통해 작동 확인.
 *  동적으로 웹서버에서 필요한 클래스들을 로드하는데 필요한 파서.
 */
package com.jeanvar.initiator;

import com.jeanvar.eventhandler.EventHandler;

/**
 * @brief			여러가지 형태의 패턴의 웹서버를 동적으로 사용하기 위한 인터페이스를 정의
 * @author TaekSoon
 *
 */
public interface WebServer {
	/**
	 * @brief			WebServer를 구동시키는 메서드.
	 */
	public void start();
	
	/**
	 * @brief			WebServer로 전달되는 메세지를 종류에 따라 처리하기 위한 핸들러를 등록하는 메서드.
	 * @param 			handle
	 * @param 			handler
	 */
	public void registerHandler(String handle, EventHandler handler);
}
