/**
 * @namespace		com.jeanvar.initiator
 * @brief			웹서버를 초기화해서 구동시키는데 필요한 요소들을 모은 패키지.
 * @details
 * 	서버를 띄우고 해당 서버에 여러번의 요청을 보내는 클라이언트를 생성.
 * 	콘솔에 출력되는 결과를 통해 작동 확인.
 *  동적으로 웹서버에서 필요한 클래스들을 로드하는데 필요한 파서.
 */
package com.jeanvar.initiator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.jeanvar.eventhandler.EventHandler;
import com.jeanvar.initiator.parser.HandlerParser;

/**
 * @brief			클래스를 동적으로 로드하는 클래스.
 * @details
 *    클래스를 동적으로 로드하는 여러가지 방법들을 정해놓은 메서드.
 *    현재는 XML 설정파일에서 클래스명을 받아오는 방법과
 *    Jar파일을 동적으로 로드하는 방법을 사용.
 * @author 			TaekSoon
 * @date			2014-05-19
 * @version		0.1
 *
 */
public class ClassLoader {
	/**
	 * @brief		XML 설정파일에서 Handle과 Handler를 불러오고 Reactor에 등록.
	 * @details
	 * 	XML 파일에서 Handle과 Handler를 파싱해서 map으로 저장.
	 *  Map안의 모든 Handler과 Handler를 인자로 전달된 Reactor에 등록.
	 * @param 		reactor : 이벤트 핸들러를 등록할 Reactor 인스턴스.
	 * @exception	Exception(Serializer가 Parser를 로드할 때)
	 * 
	 * @see			SimpleFramework 2.7.1
	 * 
	 */
	public void registerHandlerFromXML(WebServer webServer, String xmlPath) {
		Serializer serializer = new Persister();
		File sortConfigXML = new File(xmlPath);
		
		HandlerParser parser;
		try {
			parser = serializer.read(HandlerParser.class, sortConfigXML);
			Map<String, String> map = parser.getHandlerMap();
			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				String handle = (String)pairs.getKey();
				String handlerClassName = (String)pairs.getValue();
				EventHandler handler = (EventHandler)Class.forName(handlerClassName).getDeclaredConstructor(String.class).newInstance(handle);
				webServer.registerHandler(handle, handler);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @brief		등록된 Jar 파일로부터 Class를 로드함.
	 * @details	resources/lib 디렉토리에 등록된 jar 파일을 검사하여
	 * 				클래스들을 런타임에 로드함.
	 * @exception	IOException, ClassNotFoundException
	 * @see			http://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar
	 */
	public void loadClassesFromJar() {
		String pathToJar = "/Users/TaekSoon/git/sw_architect_midterm_hw/SW_Architect_Web_Server/resources/configuration/reactor/UpdateProfileEventHandler.jar";
		
		JarFile jarFile;
		try {
			jarFile = new JarFile(pathToJar);
			Enumeration e = jarFile.entries();

	        URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
	        URLClassLoader cl = URLClassLoader.newInstance(urls);

	        while (e.hasMoreElements()) {
	            JarEntry je = (JarEntry) e.nextElement();
	            if(je.isDirectory() || !je.getName().endsWith(".class")){
	                continue;
	            }
	            String className = je.getName().substring(0,je.getName().length()-6);
	            className = className.replace('/', '.');
	            Class c = cl.loadClass(className);
	        }
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}
