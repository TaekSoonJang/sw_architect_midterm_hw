/**
 * @namespace		com.jeanvar.initiator
 * @brief			웹서버를 초기화해서 구동시키는데 필요한 요소들을 모은 패키지.
 * @details
 * 	서버를 띄우고 해당 서버에 여러번의 요청을 보내는 클라이언트를 생성.
 * 	콘솔에 출력되는 결과를 통해 작동 확인.
 *  동적으로 웹서버에서 필요한 클래스들을 로드하는데 필요한 파서.
 */
package com.jeanvar.initiator;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @brief		서버로 요청을 보내는 클라이언트 테스트 클래스
 * @details	
 * 	ShowProfile과 UpdateProfile에 해당하는 메시지를 생성.
 * 	연속적으로 서버에 요청을 보냄.
 * @author 		TaekSoon
 * @date		2014-05-12
 * @version	0.1
 * 
 */
public class ClientTest {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("127.0.0.1", 5000);
			OutputStream out = socket.getOutputStream();
			String message = "0x5001|Taeksoon|22";
			out.write(message.getBytes());
			socket.close();
			
			Socket socket2 = new Socket("127.0.0.1", 5000);
			OutputStream out2 = socket2.getOutputStream();
			message = "0x9005";
			out2.write(message.getBytes());
			socket2.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
