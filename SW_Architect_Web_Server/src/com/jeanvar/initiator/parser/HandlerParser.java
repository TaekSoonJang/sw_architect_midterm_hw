/**
 * @namespace		com.jeanvar.initiator.parser
 * @brief			XML 설정파일을 불러오는데 필요한 Parser 클래스 정의.
 * @details
 * 	  WebServer의 패턴을 설정하는 WebServerParser
 * 	  WebServer에 따른 EventHandler를 설정하는 HandlerParser
 */
package com.jeanvar.initiator.parser;

import java.util.Map;

import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

/**
 * @brief		XML파일에서 handle과 handlerClass를 파싱하기 위한 클래스
 * @details
 * 	XML 설정 파일에는 복수의 <eventHandler>태그가 존재함.
 *  하위에 <handle>과 <handlerClass>태그가 있고 각각의 값을 Key, Value로 저장한
 *  HandlerMap을 가진다.
 * @author 		TaekSoon
 *
 */
@Root
public class HandlerParser {
	@ElementMap(name="handlers", entry="eventHandler", key="handle", value="handlerClass")
	private Map<String, String> handlerMap;
	
	/**
	 * @brief		Handle과 EventHandler를 가진 Map을 반환하는 메서드
	 * @return		<handle, eventHandler>로 이루어진 Map
	 */
	public Map<String, String> getHandlerMap() {
		return handlerMap;
	}
}
