/**
 * @namespace		com.jeanvar.initiator.parser
 * @brief			XML 설정파일을 불러오는데 필요한 Parser 클래스 정의.
 * @details
 * 	  WebServer의 패턴을 설정하는 WebServerParser
 * 	  WebServer에 따른 EventHandler를 설정하는 HandlerParser
 */
package com.jeanvar.initiator.parser;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @brief		XML파일에서 WebServer 패턴을 추출하는 파서 클래스.
 * @details
 * 	<webServerClass> WebServer의 클래스
 *  <handlerConfigurationPath> 해당 WebServer의 이벤트 핸들러 설정 파일의 위치.
 * @author 		TaekSoon
 *
 */
@Root
public class WebServerParser {
	@Element
	private String webServerClass;
	@Element
	private String handlerConfigurationPath;
	
	public String getWebServerClass() {
		return webServerClass;
	}
	public String getHandlerConfigurationPath() {
		return handlerConfigurationPath;
	}
}
