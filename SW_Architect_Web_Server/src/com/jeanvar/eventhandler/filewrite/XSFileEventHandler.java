package com.jeanvar.eventhandler.filewrite;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

public class XSFileEventHandler implements EventHandler {
	
	private static int FILE_SIZE = 512;
	public Logger logger = Logger.getLogger(XSFileEventHandler.class.getName());
	
	private String handle;
	
	public XSFileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public Object handleEvent(Object attachment) {
		AsyncFileWrite afw = new AsyncFileWrite(FILE_SIZE);
		
		return afw.write();
	}
}
