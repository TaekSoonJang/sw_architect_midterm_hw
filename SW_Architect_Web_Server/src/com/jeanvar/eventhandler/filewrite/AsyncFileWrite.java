package com.jeanvar.eventhandler.filewrite;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;

public class AsyncFileWrite {
	
	public Logger logger = Logger.getLogger(AsyncFileWrite.class.getName());
	
	private int fileSize;
	
	public AsyncFileWrite(int fileSize) {
		this.fileSize = fileSize;
	}
	
	public String write() {
		String responseMsg = "File Write Failed.";	// Default Message
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < fileSize; i ++) {
			sb.append("A");
		}
		
		ByteBuffer buffer = ByteBuffer.wrap(sb.toString().getBytes());
		
		long time = System.currentTimeMillis();
		
		AsynchronousFileChannel asyncChannel;
		try {
			asyncChannel = AsynchronousFileChannel.open(Paths.get("/Users/TaekSoon/Documents/File" + fileSize + System.currentTimeMillis()), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
			Future<Integer> fileResult = asyncChannel.write(buffer, 0);
			while(!fileResult.isDone()){
				logger.info("Waiting to complete the file writing ...");
			}
			logger.info("Number of bytes written: "+fileResult.get());
			responseMsg = fileResult.get() + "bytes written";
			logger.info("time:"+(System.currentTimeMillis()-time));
			asyncChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} finally {
			return responseMsg;
		}
	}
}
