package com.jeanvar.eventhandler.filewrite;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

public class XLFileEventHandler implements EventHandler {

	private static int FILE_SIZE = 20000000;		// 20M
	public Logger logger = Logger.getLogger(XLFileEventHandler.class.getName());
	
	private String handle;
	
	public XLFileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public Object handleEvent(Object attachment) {
		AsyncFileWrite afw = new AsyncFileWrite(FILE_SIZE);
		
		return afw.write();
	}
}
