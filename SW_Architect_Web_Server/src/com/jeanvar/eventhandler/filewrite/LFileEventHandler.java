package com.jeanvar.eventhandler.filewrite;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

public class LFileEventHandler implements EventHandler {

	private static int FILE_SIZE = 2000000;		// 2M
	public Logger logger = Logger.getLogger(LFileEventHandler.class.getName());
	
	private String handle;
	
	public LFileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public Object handleEvent(Object attachment) {
		AsyncFileWrite afw = new AsyncFileWrite(FILE_SIZE);
		
		return afw.write();
	}
}
