package com.jeanvar.eventhandler.filewrite;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

public class MFileEventHandler implements EventHandler {

	private static int FILE_SIZE = 200000;		// 200k
	public Logger logger = Logger.getLogger(MFileEventHandler.class.getName());
	
	private String handle;
	
	public MFileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public Object handleEvent(Object attachment) {
		AsyncFileWrite afw = new AsyncFileWrite(FILE_SIZE);
		
		return afw.write();
	}
}
