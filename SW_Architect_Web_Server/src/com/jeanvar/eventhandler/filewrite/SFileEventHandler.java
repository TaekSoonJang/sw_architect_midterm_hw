package com.jeanvar.eventhandler.filewrite;

import org.apache.log4j.Logger;

import com.jeanvar.eventhandler.EventHandler;

public class SFileEventHandler implements EventHandler {

	private static int FILE_SIZE = 3000;
	public Logger logger = Logger.getLogger(SFileEventHandler.class.getName());
	
	private String handle;
	
	public SFileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public Object handleEvent(Object attachment) {
		AsyncFileWrite afw = new AsyncFileWrite(FILE_SIZE);
		
		return afw.write();
	}
}
