/**
 * @namespace		com.jeanvar.reactor.eventhandler
 * @brief			reactor 패턴에서 사용되는 event handler와 관련된 패키지
 * @details
 * 	EventHandler의 인터페이스와 구현 클래스,
 *  그리고 이벤트 핸들러 설정 XML파일을 파싱할 파서 클래스를 정의.
 */
package com.jeanvar.eventhandler.reactor;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.StringTokenizer;

import com.jeanvar.eventhandler.EventHandler;

/**
 * @brief		Profile을 Update하는 EventHandler
 * @details
 * 	메시지 : |name|age|position|money|address|
 * 	각각의 내용을 파싱해서 업데이트함.
 * @author 		TaekSoon
 * @date		2014-05-12
 * @version	0.1
 * 
 * @see			EventHandler 사용 검증을 위한 임시 클래스
 *
 */
public class UpdateProfileEventHandler implements EventHandler {
	private static final int DATA_SIZE = 1024;
	private static final int TOKEN_NUM = 5;
	
	private String handle;
	
	public UpdateProfileEventHandler(String handle) {
		this.handle = handle;
	}
	
	public String getHandle() {
		return handle;
	}

	@Override
	public Object handleEvent(Object clientSocket) {
		Socket cs = (Socket)clientSocket;
		try {
			InputStream is = cs.getInputStream();
			byte[] buf = new byte[DATA_SIZE];
			is.read(buf);
			String data = new String(buf);
			
			String[] params = new String[TOKEN_NUM];
			StringTokenizer token = new StringTokenizer(data, "|");
			
			int i = 0;
			while (token.hasMoreTokens()) {
				params[i] = token.nextToken();
				++i;
			}
			
			updateProfile(params);
			is.close();
			cs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return null;
		}
	}

	private void updateProfile(String[] params) {
		for (int i = 0; i < TOKEN_NUM; i++) {
			System.out.println(params[i]);
		}
	}
}
