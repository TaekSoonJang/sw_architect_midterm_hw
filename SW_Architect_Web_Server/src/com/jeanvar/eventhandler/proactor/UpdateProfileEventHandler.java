/**
 * @namespace		com.jeanvar.proactor.eventhandler
 * @brief			proactor 패턴에서 사용되는 event handler와 관련된 패키지
 * @details
 * 	EventHandler의 인터페이스와 구현 클래스,
 *  그리고 이벤트 핸들러 설정 XML파일을 파싱할 파서 클래스를 정의.
 */
package com.jeanvar.eventhandler.proactor;

import java.util.StringTokenizer;

import com.jeanvar.eventhandler.EventHandler;

/**
 * @brief		Profile을 Update하는 EventHandler
 * @details
 * 	메시지 : |name|age|position|money|address|
 * 	각각의 내용을 파싱해서 업데이트함.
 * @author 		TaekSoon
 * @date		2014-05-19
 * @version	0.1
 * 
 * @see			EventHandler 사용 검증을 위한 임시 클래스
 *
 */
public class UpdateProfileEventHandler implements EventHandler {
	private static final int TOKEN_NUM = 5;
	private String handle;
	
	public UpdateProfileEventHandler(String handle) {
		this.handle = handle;
	}
	
	@Override
	public String handleEvent(Object bodyString) {
		String body = (String)bodyString;
		String[] params = new String[TOKEN_NUM];
		StringTokenizer token = new StringTokenizer(body, "|");
		
		int i = 0;
		while (token.hasMoreTokens()) {
			params[i] = token.nextToken();
			++i;
		}
		
		String responseString = "Msg : ";
		for (i = 0; i < TOKEN_NUM; i++) {
			responseString += params[i] + " ";
		}
		
		return responseString;
	}
}
