/**
 * @namespace		com.jeanvar.eventhandler
 * @brief			웹서버로 오는 message를 처리하는데 사용되는 event handler와 관련된 패키지
 * 
 */
package com.jeanvar.eventhandler;

/**
 * @brief		Eventhandler의 형태를 정의한 인터페이스
 * @author 		TaekSoon
 * @date		2014-05-19
 * @version	0.9
 * 
 */
public interface EventHandler {
	public Object handleEvent(Object attachment);
}
